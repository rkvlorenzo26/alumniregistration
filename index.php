<?php 
include_once 'connection.php';
?>
<!DOCTYPE html>
<html>
<title>HOME</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/w3.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/css.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="icon" href="img/stilogo.png">
<link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/snackbar.css">

<script src="reports/assets/js/jquery-1.12.4.min.js"></script>
<script src="reports/assets/canvasjs.min.js"></script>
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif}
body, html {
    height: 100%;
    line-height: 1.8;
}
/* Full height image header */
.bgimg-1 {
    background-position: center;
    background-size: cover;
    background-image: url("/w3images/mac.jpg");
    min-height: 100%;
}
.w3-bar .w3-button {
    padding: 16px;
}
</style>
<body>

<!-- Navbar (sit on top) -->
<div class="w3-top">
  <div class="w3-bar w3-sti w3-card-4" id="myNavbar">
    <img style="margin-left: 20px;" src="img/stilogo.png"></img> 
    <div class="w3-right w3-hide-small">
      <a href="index.php" class="w3-hover-stiyellow w3-bar-item w3-button">HOME</a>
      <a href="alumni.php" class="w3-hover-stiyellow w3-bar-item w3-button">ALUMNI</a>
      <a href="faculty&staff.php" class="w3-hover-stiyellow w3-bar-item w3-button">FACULTY & STAFF</a>
    </div>
    </a>
  </div>
</div>

<?php
	$a = mysqli_query($con,"Select * from tbl_registration");
	$count = mysqli_num_rows($a);
?>

<div class="w3-row">
  <div class="w3-col m6 w3-row-padding w3-padding-64 w3-container">
    <br><br>
    <div class="w3-card-4">
      <header class="w3-container w3-white">
        <h3>Alumni attendance based on batch</h3>
        <hr>
      </header>
      <div class="w3-container">
      	<strong><p class="pull-right ">Total no. of attendees: <?php echo $count; ?></p></strong>
      	<br><br>
      	<div id="chartBatch"></div>
      	<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
      	<?php 
      		$batch = mysqli_query($con,"SELECT Batch, count(Batch) as count from tbl_registration GROUP BY Batch ORDER BY Batch DESC");
      		$dataPoints = array(); 

      		while($row = mysqli_fetch_array($batch)){
                $arr = array("y" => $row['count'], "label" => $row['Batch']);
                array_push($dataPoints,$arr);
      		}
      	?>
        <script type="text/javascript">
            $(function () {
                var chart = new CanvasJS.Chart("chartBatch", {
                animationEnabled: true,
                title: {
                text: ""
                },
				data: [{
					type: "column",
					dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
				}]
                });
                     chart.render();
                });
        </script>
      </div> 
      <br>
    </div>
  </div>

    <div class="w3-col m6 w3-row-padding w3-padding-64 w3-container">
    <br><br>
    <div class="w3-card-4">
      <header class="w3-container w3-white">
        <h3>Alumni attendance based on courses</h3>
        <hr>
      </header>
      <div class="w3-container">
      	<strong><p class="pull-right ">Total no. of attendees: <?php echo $count; ?></p></strong>
      	<br><br>
      	<div id="chartCourse"></div>
      	<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
      	<?php 
      		$course = mysqli_query($con,"SELECT Course, count(Course) as count from tbl_registration GROUP BY Course");
      		$dataPoints = array(); 

      		while($row = mysqli_fetch_array($course)){
      			$query = mysqli_query($con,"Select * from courses where ID = '$row[Course]'");

      			$a = mysqli_fetch_array($query);

                $arr = array("y" => $row['count'], "label" => $a['Course']);
                array_push($dataPoints,$arr);
      		}
      	?>
        <script type="text/javascript">
            $(function () {
                var chart = new CanvasJS.Chart("chartCourse", {
                theme: "theme2",
                animationEnabled: true,
                title: {
                text: ""
                },
                data: [
                {
                    type: "pie",           
                    dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                }
                    ]
                });
                     chart.render();
                });
        </script>
      </div> 
      <br>
    </div>
  </div>
</div>

  <div class="w3-center w3-padding-36">
    STI SOUTHWOODS ALUMNI SPORSTFEST 2018
  </div>

</body>
</html>
