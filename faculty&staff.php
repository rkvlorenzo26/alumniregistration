<?php 
include_once 'connection.php';
?>
<!DOCTYPE html>
<html>
<title>FACULTY & STAFF REGISTRATION</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/w3.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/css.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="icon" href="img/stilogo.png">
<link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/snackbar.css">
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif}
body, html {
    height: 100%;
    line-height: 1.8;
}
/* Full height image header */
.bgimg-1 {
    background-position: center;
    background-size: cover;
    background-image: url("/w3images/mac.jpg");
    min-height: 100%;
}
.w3-bar .w3-button {
    padding: 16px;
}

#toast {
    visibility: hidden;
    max-width: 50px;
    height: 50px;
    /*margin-left: -125px;*/
    margin: auto;
    background-color: #333;
    color: #fff;
    text-align: center;
    border-radius: 2px;

    position: fixed;
    z-index: 1;
    left: 0;right:0;
    bottom: 30px;
    font-size: 17px;
    white-space: nowrap;
}
#toast #img{
  width: 50px;
  height: 50px;
    
    float: left;
    
    padding-top: 16px;
    padding-bottom: 16px;
    
    box-sizing: border-box;

    
    background-color: #111;
    color: #fff;
}
#toast #desc{

    
    color: #fff;
   
    padding: 16px;
    
    overflow: hidden;
  white-space: nowrap;
}

#toast.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, expand 0.5s 0.5s,stay 3s 1s, shrink 0.5s 2s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, expand 0.5s 0.5s,stay 3s 1s, shrink 0.5s 4s, fadeout 0.5s 4.5s;
}

@-webkit-keyframes fadein {
    from {bottom: 0; opacity: 0;} 
    to {bottom: 30px; opacity: 1;}
}

@keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
}

@-webkit-keyframes expand {
    from {min-width: 50px} 
    to {min-width: 450px}
}

@keyframes expand {
    from {min-width: 50px}
    to {min-width: 450px}
}
@-webkit-keyframes stay {
    from {min-width: 450px} 
    to {min-width: 450px}
}

@keyframes stay {
    from {min-width: 450px}
    to {min-width: 450px}
}
@-webkit-keyframes shrink {
    from {min-width: 450px;} 
    to {min-width: 50px;}
}

@keyframes shrink {
    from {min-width: 450px;} 
    to {min-width: 50px;}
}

@-webkit-keyframes fadeout {
    from {bottom: 30px; opacity: 1;} 
    to {bottom: 60px; opacity: 0;}
}

@keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 60px; opacity: 0;}
}
</style>
<body>

<!-- Navbar (sit on top) -->
<div class="w3-top">
  <div class="w3-bar w3-sti w3-card-4" id="myNavbar">
    <img style="margin-left: 20px;" src="img/stilogo.png"></img> 
    <div class="w3-right w3-hide-small">
      <a href="index.php" class="w3-hover-stiyellow w3-bar-item w3-button">HOME</a>
      <a href="alumni.php" class="w3-hover-stiyellow w3-bar-item w3-button">ALUMNI</a>
      <a href="faculty&staff.php" class="w3-hover-stiyellow w3-bar-item w3-button">FACULTY & STAFF</a>
    </div>
    </a>
  </div>
</div>

<div class="w3-row">
  <div class="w3-col m6 col-md-offset-3 w3-row-padding w3-padding-64 w3-container">
    <br><br>
    <div class="w3-card-4">
      <header class="w3-container w3-white">
        <h3>Faculty & Staff - Registration Form</h3>
        <hr>
      </header>
      <div class="w3-container">
        <form id="regForm" class="w3-container">

          <div id="error"></div>

          <p><label>Last Name</label>
          <input id="lname" name="lname" class="w3-input w3-border" placeholder="Enter your last name" type="text"></p>
          
          <p><label>First Name</label>
          <input id="fname" name="fname" class="w3-input w3-border" placeholder="Enter your first name" type="text"></p>

          <p><label>Middle Name (Optional)</label>
          <input id="mname" name="mname" class="w3-input w3-border" placeholder="Enter your middle name" type="text"></p>

          <a id="btnreg" href="javascript:void(0);" onclick="register()" class="w3-btn w3-hover-stiyellow w3-block w3-sti">Register</a>
        </form>
      </div> 
      <br>
    </div>
  </div>
</div>

  <div class="w3-center w3-padding-36">
    STI SOUTHWOODS ALUMNI SPORSTFEST 2018
  </div>

<!-- The actual toast -->
<div id="toast"><div id="img"><span class="fa fa-check"></span></div><div id="desc">You have registered successfully. Thank you!</div></div>

<script src="js/jquery-1.10.2.js" type="text/javascript"></script>

<script>
  
function register(){
  document.getElementById("btnreg").disabled = true;

  var fname = document.getElementById('fname').value;
  var lname = document.getElementById('lname').value;

  if(fname == "" || lname == ""){
    check_validations();
  }else{
    saveInfo();
  }
}

function check_validations(){
    var div = document.getElementById('error');

    var obj = '<div class="w3-panel w3-red">';
    obj += '<h5>Please fill up all fields!</h5>';
    obj += '</div>';

    div.innerHTML = obj;
}

function launch_toast() {
  var x = document.getElementById("toast")
  x.className = "show";
  setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);
}

function reset_form(){
  document.getElementById("btnreg").disabled = false;
  document.getElementById("regForm").reset();

  var div = document.getElementById('error');
  div.innerHTML = "";
}

function saveInfo(){
  var form = $('#regForm');

  console.log(form.serialize());

  $.ajax({
    url: 'functions/save_faculty&staff.php',
    type: 'POST',
    data: form.serialize(),
    success: function(success){
      if(!success.error){
          var status = success;
          console.log(success);
          if(status == "success"){
            launch_toast();
            reset_form();
          }
      }
    }
  });
}

</script>  

</body>
</html>
