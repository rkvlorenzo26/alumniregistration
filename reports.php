<?php 
include_once 'connection.php';
?>
<!DOCTYPE html>
<html>
<title>STI SOUTHWOODS</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/w3.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/css.css">
<link rel="icon" href="img/stilogo.png">
<link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/snackbar.css">

<script src="reports/assets/js/jquery-1.12.4.min.js"></script>
<script src="reports/assets/canvasjs.min.js"></script>
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif}
body, html {
    height: 100%;
    line-height: 1.8;
}
/* Full height image header */
.bgimg-1 {
    background-position: center;
    background-size: cover;
    background-image: url("/w3images/mac.jpg");
    min-height: 100%;
}
.w3-bar .w3-button {
    padding: 16px;
}
</style>
<body>

<!-- Navbar (sit on top) -->
<div class="w3-top">
  <div class="w3-bar w3-sti w3-card-4" id="myNavbar">
    <img style="margin-left: 20px;" src="img/stilogo.png"></img> 
    <div class="w3-right w3-hide-small">
      <button class="w3-bar-item w3-hover-stiyellow w3-button" onclick="openCity('London')">Alumni</button>
      <button class="w3-bar-item w3-hover-stiyellow w3-button" onclick="openCity('Paris')">Faculty&Staff</button>
      <button class="w3-bar-item w3-hover-stiyellow w3-button" onclick="openCity('Tokyo')">Guests</button>
    </div>
  </div>
</div>

<div id="London" class="w3-container city">
  <div class="w3-row">
  <div class="w3-col m12 w3-row-padding w3-padding-64 w3-container">
    <h2>Alumni</h2>
    <!--
    <button class="btn btn-primary pull-right" id="btnExport" onclick="download_alumni();"><span class="fa fa-file"> Export to XLS</span> </button> 
  -->
    <br><br>
    <table id="example1" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>Name</th>
          <th>Batch</th>
          <th>Course</th>
          <th>Time In</th>
        </tr>
      </thead>        
      <tbody>
      <?php
        $alumni = mysqli_query($con,"SELECT * from tbl_registration");
        while($row = mysqli_fetch_array($alumni)){
          $courses = mysqli_query($con,"Select * from courses where ID = '$row[Course]'");
          $a = mysqli_fetch_array($courses);

          $date = date('m-d-Y h:i:s', strtotime($row['TimeIn']));
      ?>
        <tr>
          <td><?php echo $row['ID']; ?></td>
          <td><?php echo $row['Lastname'].", ".$row['Firstname']." ".$row['Middlename']; ?></td>
          <td><?php echo $row['Batch']; ?></td>
          <td><?php echo $a['Course']; ?></td>
          <td><?php echo $date; ?></td>
        </tr>
      <?php    
        }
      ?>

      </tbody>
    </table>
  </div>
  </div>
</div>

<div id="Paris" class="w3-container city" style="display:none">
  <div class="w3-row">
  <div class="w3-col m12 w3-row-padding w3-padding-64 w3-container">
    <h2>Faculty & Staff</h2>
    <!--
    <button class="btn btn-primary pull-right" id="btnExport" onclick="download_faculty_staff();"><span class="fa fa-file"> Export to XLS</span> </button> 
  -->
    <br><br>
    <table id="example2" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>Name</th>
          <th>Time In</th>
        </tr>
      </thead>
      <tbody>
        <?php
          $faculty_staff = mysqli_query($con, "SELECT * from tbl_faculty_staff");
          while($row = mysqli_fetch_array($faculty_staff)){
            $date = date('m-d-Y h:i:s', strtotime($row['TimeIn']));
        ?>
          <tr>
          <td><?php echo $row['ID']; ?></td>
          <td><?php echo $row['Lastname'].', '.$row['Firstname'].' '.$row['Middlename']; ?></td>
          <td><?php echo $date; ?></td>
          </tr>
        <?php
          }
        ?>
      </tbody>
    </table>
  </div>
  </div>
</div>

<div id="Tokyo" class="w3-container city" style="display:none">
  <div class="w3-row">
  <div class="w3-col m12 w3-row-padding w3-padding-64 w3-container">
    <h2>Guests</h2>
    <!--
    <button class="btn btn-primary pull-right" id="btnExport" onclick="download_guests();"><span class="fa fa-file"> Export to XLS</span> </button> 
  -->
    <br><br>
    <table id="example3" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>No.</th>
          <th>Guest's Name</th>
          <th>Alumni's Name</th>
          <th>Relationship</th>
          <th>Time In</th>
        </tr>
      </thead>
      <tbody>
        <?php
          $guests = mysqli_query($con,"SELECT * from tbl_guest");
          while($row = mysqli_fetch_array($guests)){
            $alum = mysqli_query($con, "SELECT * from tbl_registration where ID = '$row[Alumni]'");
            $a = mysqli_fetch_array($alum);

            $date = date('m-d-Y h:i:s', strtotime($row['TimeIn']));
        ?>
          <tr>
          <td><?php echo $row['ID']; ?></td>
          <td><?php echo $row['Name']; ?></td>
          <td><?php echo $a['Lastname'].', '.$a['Firstname'].' '.$a['Middlename']; ?></td>
          <td><?php echo $row['Relationship']; ?></td>
          <td><?php echo $date; ?></td>
          </tr>
        <?php
          }
        ?>        

      </tbody>
    </table>
  </div>
  </div>
</div>

  <div class="w3-center w3-padding-36">
    STI SOUTHWOODS ALUMNI SPORSTFEST 2018
  </div>
<script>
function openCity(cityName) {
    var i;
    var x = document.getElementsByClassName("city");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    document.getElementById(cityName).style.display = "block";  
}
</script> 


<link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable()
    $('#example3').DataTable()
  })
</script>
<script>
  function download_alumni(){
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#example1').html()));
    event.preventDefault();
  }

  function download_faculty_staff(){
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#example2').html()));
    event.preventDefault();
  }

  function download_guests(){
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#example3').html()));
    event.preventDefault();
  }

</script>

</body>
</html>
